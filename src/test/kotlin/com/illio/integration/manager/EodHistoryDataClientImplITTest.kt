package com.illio.integration.manager

import com.illio.manager.EodHistoryDataClientImpl
import com.illio.manager.dto.StockDto
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.hasSize
import strikt.assertions.isEmpty
import strikt.assertions.isNotEmpty
import java.time.LocalDate

internal class EodHistoryDataClientImplITTest {

	private val underTest = EodHistoryDataClientImpl()

	@Test
	fun `should return 1 week eod price`() {

		runBlocking {
			//when
			val result: List<StockDto> = underTest.getEodHistoryData(LocalDate.of(2022, 4, 4), LocalDate.of(2022, 4, 8), "AAPL.US")

			expectThat(result) {
				isNotEmpty()
				hasSize(5)
			}
		}
	}

	@Test
	fun `abnormal start & end`() {

		runBlocking {
			//when
			val result: List<StockDto> = underTest.getEodHistoryData(LocalDate.of(2022, 4, 4), LocalDate.of(2022, 4, 1), "AAPL.US")

			expectThat(result) {
				isEmpty()
			}
		}
	}

	@Test
	fun `abnormal ticker`() {

		runBlocking {
			//when
			val result: List<StockDto> = underTest.getEodHistoryData(LocalDate.of(2022, 4, 4), LocalDate.of(2022, 4, 5), "DUMMY.US")

			expectThat(result) {
				isEmpty()
			}
		}
	}
}

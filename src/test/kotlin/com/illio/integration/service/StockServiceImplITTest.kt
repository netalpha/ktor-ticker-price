package com.illio.integration.service

import com.illio.manager.EodHistoryDataClientImpl
import com.illio.manager.FxStubClientImpl
import com.illio.service.StockService
import com.illio.service.StockServiceImpl
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import java.time.LocalDate
import java.util.*

internal class StockServiceImplITTest {

	private val underTest: StockService = StockServiceImpl(EodHistoryDataClientImpl(), FxStubClientImpl())

    @Test
    fun `base case - get 5 days close price and calc AVG`() {

		runBlocking {
			val result = underTest.getAveragePriceInPennies(LocalDate.of(2022, 4, 4), LocalDate.of(2022, 4, 8), "AAPL.US")

			expectThat(result) {
				get { priceInPennies }.isEqualTo(17351)
				get { currencyCode }.isEqualTo("USD")
			}
		}
    }

	@Test
	fun `get 0 days close price and calc AVG`() {

		runBlocking {
			val result = underTest.getAveragePriceInPennies(LocalDate.of(2022, 4, 4), LocalDate.of(2022, 4, 3), "AAPL.US")

			expectThat(result) {
				get { priceInPennies }.isEqualTo(0)
				get { currencyCode }.isEqualTo("USD")
			}
		}
	}

	@Test
	fun `base case - get 5 days close price and calc AVG in GBP`() {

		runBlocking {
			val result = underTest.getAveragePriceInPenniesByCurrency(LocalDate.of(2022, 4, 4), LocalDate.of(2022, 4, 8), "AAPL.US", Currency.getInstance("GBP"))

			expectThat(result) {
				get { priceInPennies }.isEqualTo(34702)
				get { currencyCode }.isEqualTo("GBP")
			}
		}
	}

	@Test
	fun `base case - get 5 days close price and calc AVG in EUR`() {

		runBlocking {
			val result = underTest.getAveragePriceInPenniesByCurrency(LocalDate.of(2022, 4, 4), LocalDate.of(2022, 4, 8), "AAPL.US", Currency.getInstance("EUR"))

			expectThat(result) {
				get { priceInPennies }.isEqualTo(52054)
				get { currencyCode }.isEqualTo("EUR")
			}
		}
	}
}

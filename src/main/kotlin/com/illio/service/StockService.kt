package com.illio.service

import com.illio.domain.StockPrice
import java.time.LocalDate
import java.util.*

interface StockService {
	suspend fun getAveragePriceInPennies(from: LocalDate, to: LocalDate, ticker: String): StockPrice

	suspend fun getAveragePriceInPenniesByCurrency(from: LocalDate, to: LocalDate, ticker: String, currency: Currency): StockPrice
}

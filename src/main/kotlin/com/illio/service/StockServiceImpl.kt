package com.illio.service

import com.illio.domain.StockPrice
import com.illio.manager.EodHistoryDataClient
import com.illio.manager.FxStubClient
import java.time.LocalDate
import java.util.*
import kotlin.math.roundToInt

class StockServiceImpl(
	private val eodHistoryDataClient: EodHistoryDataClient,
	private val fxStubClient: FxStubClient
) : StockService {

	override suspend fun getAveragePriceInPennies(from: LocalDate, to: LocalDate, ticker: String): StockPrice {
		val stockAvgPriceInUsd = getAveragePrice(from, to, ticker).roundToInt()
		return StockPrice(stockAvgPriceInUsd, "USD")
	}

	override suspend fun getAveragePriceInPenniesByCurrency(
		from: LocalDate,
		to: LocalDate,
		ticker: String,
		currency: Currency
	): StockPrice {
		val stockAvgPrice = this.getAveragePrice(from, to, ticker)
		val fxRate = this.fxStubClient.getFxRate(currency)

		val stockAvgPriceInCurrency = stockAvgPrice.times(fxRate).roundToInt()
		return StockPrice(stockAvgPriceInCurrency, currency.currencyCode)
	}

	private suspend fun getAveragePrice(from: LocalDate, to: LocalDate, ticker: String): Double {
		val stocks = eodHistoryDataClient.getEodHistoryData(from, to, ticker)

		if (stocks.isEmpty()) return 0.0

		return stocks
			.sumOf { it.close }
			.div(stocks.size)
			.toPennies()
	}

	private fun Double.toPennies() = this.times(100)
}

package com.illio.domain

data class StockPrice(val priceInPennies: Int, val currencyCode: String)

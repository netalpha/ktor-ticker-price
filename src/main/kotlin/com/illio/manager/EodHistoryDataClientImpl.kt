package com.illio.manager

import com.illio.manager.dto.StockDto
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.request.*
import io.ktor.serialization.gson.*
import java.time.LocalDate

class EodHistoryDataClientImpl(
	private val apiToken: String = "OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX",
	private val baseUrl: String = "https://eodhistoricaldata.com/api/eod"
) : EodHistoryDataClient {

	private val client = HttpClient(CIO) {
		expectSuccess = true

		install(Logging) {
			logger = Logger.DEFAULT
			level = LogLevel.HEADERS
		}

		install(HttpRequestRetry) {
			retryOnServerErrors(maxRetries = 5)
			exponentialDelay()
		}

		install(ContentNegotiation) {
			gson()
		}
	}

	override suspend fun getEodHistoryData(from: LocalDate, to: LocalDate, ticker: String): List<StockDto> {

		return try {
			val stocks: List<StockDto> = client.get("$baseUrl/$ticker") {
				parameter("api_token", apiToken)
				parameter("period", "d")
				parameter("from", from)
				parameter("to", to)
				parameter("fmt", "json")
			}.body()

			return stocks
		} catch (ex: Exception) {
			emptyList()
		}

	}
}

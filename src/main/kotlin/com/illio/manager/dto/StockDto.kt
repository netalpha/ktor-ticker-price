package com.illio.manager.dto

data class StockDto(val close: Double)

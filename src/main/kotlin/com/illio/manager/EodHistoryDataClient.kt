package com.illio.manager

import com.illio.manager.dto.StockDto
import java.time.LocalDate

interface EodHistoryDataClient {
	suspend fun getEodHistoryData(from: LocalDate, to: LocalDate, ticker: String): List<StockDto>
}

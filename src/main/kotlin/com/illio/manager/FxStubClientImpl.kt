package com.illio.manager

import java.util.*

class FxStubClientImpl: FxStubClient {

	/**
	 * a mock to simulate an external fx exchange service
	 */
	override fun getFxRate(to: Currency): Double {
		return if ("GBP" == to.currencyCode) {
			2.0
		} else {
			3.0
		}
	}
}

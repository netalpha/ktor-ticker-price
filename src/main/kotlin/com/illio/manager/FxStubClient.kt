package com.illio.manager

import java.util.*

interface FxStubClient {

	fun getFxRate(to: Currency): Double
}
